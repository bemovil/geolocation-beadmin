import React from "react";
import ReactDOM from "react-dom";
// import MapLeaflet from "./components/MapLeaflet";
import "./styles.css";
import "leaflet/dist/leaflet.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";


import UploadFiles from "./components/upload-files.component";

function App() {
  return (
    <section className="Apies">
      <main className="main">
        {/* <h2>Map</h2>
        <MapLeaflet /> */}
        <div className="container" style={{ width: "600px" }}>
          <div style={{ margin: "20px" }}>
            <h4>Show locations from Juridica and Natural</h4>
          </div>

          <UploadFiles />
        </div>
      </main>
    </section>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
