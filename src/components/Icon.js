import L from 'leaflet';

const iconPerson = new L.Icon({
    iconUrl: require('../assets/pin.webp'),
    iconRetinaUrl: require('../assets/pin.webp'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(30, 35),
    className: 'leaflet-div-icon'
});

export { iconPerson };