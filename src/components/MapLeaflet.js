// import "./Map.css";
import React, { Component } from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet";

const customMarker = new L.icon({
  iconUrl: "https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon.png",
  iconSize: [25, 41],
  iconAnchor: [13, 0]
});

export default class MapLeaflet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13
    };
  }
  // componentDidMount() {
  //   const apiUrl = 'http://127.0.0.1:8000/erp/Geolocation/';
  //   fetch(apiUrl)
  //     .then((response) => response.json())
  //     .then((data) => console.log('This is your data', data));
  // }
  setData() {

    const data = this.props.data
    // console.log(data)
    return data.flatMap(d => [{ lat: d[0], lng: d[1], addr: d[2] }])
  }

  render() {
    const positions = this.setData()
    // const position = [this.state.lat, this.state.lng];
    return (
      <div id="map">
        <MapContainer style={{ height: "100vh" }} center={[this.props.data[0][0], this.props.data[0][1]]} zoom={6}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          />
          {positions.map(({ lat, lng, addr }, index) => (
            <Marker position={[lat, lng]} icon={customMarker} key={index}>
              <Popup>
                address:{addr}

              </Popup>
            </Marker>
          ))}
        </MapContainer>
      </div>
    );
  }
}
