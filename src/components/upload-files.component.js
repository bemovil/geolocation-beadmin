import React, { Component } from "react";
import UploadService from "../services/upload-files.service";
import MapLeaflet from "./MapLeaflet"
import Loader from "react-loader-spinner";
import http from "../http-common";

// let url

export default class UploadFiles extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedFiles: undefined,
            currentFile: undefined,
            progress: 0,
            message: false,
            data: [],
            isFetching: false

        };
        this.selectFile = this.selectFile.bind(this);
        this.upload = this.upload.bind(this);


    }
    selectFile(event) {
        this.setState({
            selectedFiles: event.target.files,
        });
    }
    // url = " http://127.0.0.1:8000/api/Geolocation/"
    fetchData = () => {
        this.setState({ ...this.state, isFetching: true, message: true });
        http.get("api/GeolocationList/")
            // headers: {
            //     "Content-Type": "multipart/form-data",
            // }
            .then(response => {
                this.setState({
                    data: response.data.data, isFetching: false, message: false
                })
            })
            .catch(e => {
                console.log(e);
                this.setState({ ...this.state, isFetching: false });
            })
    };

    upload() {
        let currentFile = this.state.selectedFiles[0];

        this.setState({
            progress: 0,
            message: true,

            currentFile: currentFile,
        });

        UploadService.upload(currentFile, (event) => {
            this.setState({
                progress: Math.round((100 * event.loaded) / event.total),
            });
        })
            .then((response) => {
                // console.log(response.data.data)
                // const final = response.data.data
                this.setState({
                    data: response.data.data,
                    message: false
                });
                // return (
                //     final.map(loc => console.log(loc))
                // );
            })
            .then((files) => {
                this.setState({
                    fileInfos: files.data,

                });
            })
            .catch(() => {
                this.setState({
                    progress: 0,
                    currentFile: undefined,
                });
            });

        this.setState({
            selectedFiles: undefined,
        });
    }
    // componentDidMount() {
    //     UploadService.getFiles().then((response) => {
    //         this.setState({
    //             fileInfos: response.data,
    //         });
    //     });
    // }
    render() {
        const {
            selectedFiles,
            currentFile,
            progress,
            message,
            data
        } = this.state;

        return (
            <div>
                {/* {currentFile && (
                    <div className="progress">
                        <div
                            className="progress-bar progress-bar-info progress-bar-striped"
                            role="progressbar"
                            aria-valuenow={progress}
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{ width: progress + "%" }}
                        >
                            {progress}%
                </div>
                    </div>
                )}

                <label className="btn btn-default">
                    <input type="file" onChange={this.selectFile} />
                </label> */}

                <button className="btn btn-success"
                    // disabled={!selectedFiles}
                    onClick={this.fetchData}
                >
                    Show Map
            </button>

                {/* <div className="alert alert-light" role="alert">
                    {message}
                </div> */}

                {message && (<Loader type="Rings" color="#158522" height={350} width={500} />)}
                {data[0] && (<MapLeaflet data={data} />)}

                {/* <div className="card">
                    <div className="card-header">List of Files</div>
                    <ul className="list-group list-group-flush">
                        {fileInfos &&
                            fileInfos.map((file, index) => (
                                <li className="list-group-item" key={index}>
                                    <a href={file.url}>{file.name}</a>
                                </li>
                            ))}
                    </ul>
                </div> */}
            </div>
        );
    }
}
